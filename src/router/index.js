import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: () => import('@/pages/PageHome'),
  },
  {
    path: '/registration',
    name: 'registration',
    component: () => import('@/pages/PageRegistration'),
  },
  {
    path: '/authorization',
    name: 'authorization',
    component: () => import('@/pages/PageAuthorization'),
  },
  {
    path: '/upload',
    name: 'upload',
    component: () => import('@/pages/PageUpload'),
  },
  {
    path: '/labs/:id',
    name: 'lab',
    props: true,
    component: () => import('@/pages/PageLab'),
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
})

export default router
