import qs from 'qs'

import { searchTasks } from '@/api/data'
import filters from '@/store/modules/search/filters'

export default {
  namespaced: true,
  state: () => ({
    isLoading: false,
    tasks: [],
    totalCount: 0,
    input: null,
  }),
  getters: {
    searchParams: (state, getters) => ({
      ...getters['filters/filterParams'],
      ...(state.input && { input: state.input }),
    }),
  },
  mutations: {
    setIsLoading(state, v) {
      state.isLoading = v
    },
    setTasks(state, v) {
      state.tasks = v
    },
    setInput(state, v) {
      state.input = v
    },
  },
  actions: {
    async loadTasks({ commit, getters }) {
      try {
        commit('setIsLoading', true)
        const { data } = await searchTasks({
          params: getters.searchParams,
          paramsSerializer: (params) =>
            qs.stringify(params, { arrayFormat: 'repeat' }),
        })

        commit('setTasks', data.items)
        commit('filters/setFilters', data.filters)
      } finally {
        commit('setIsLoading', false)
      }
    },
  },
  modules: { filters },
}
