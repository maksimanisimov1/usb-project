export default {
  namespaced: true,
  state: () => ({
    filters: [
      {
        id: 'faculty',
        name: 'Факультет',
        options: [
          { id: 'fccpi', name: 'ФККПІ' },
          { id: 'flsk', name: 'ФЛСК' },
        ],
      },
      {
        id: 'speciality',
        name: 'Спеціальність',
        options: [
          {
            id: 'ing',
            name: 'Інженер',
          },
          {
            id: 'prog',
            name: 'Програміст',
          },
        ],
      },
      {
        id: 'subject',
        name: 'Спеціальність',
        options: [
          {
            id: 'math',
            name: 'Вишка',
          },
          {
            id: 'history',
            name: 'Історія',
          },
        ],
      },
    ],
    selectedFilters: [],
  }),
  getters: {
    filterIds: (state) => state.filters.map((filter) => filter.id),
    filterParams(state, getters) {
      return getters.filterIds.reduce((acc, filterId) => {
        const filterValues = state.selectedFilters.filter(
          (filter) => filter.id === filterId
        )
        return {
          ...acc,
          [filterId]: filterValues.map((filter) => filter.optionId),
        }
      }, {})
    },
  },
  mutations: {
    setFilters(state, v) {
      state.filters = v
    },
    setSelectedFilters(state, v) {
      state.selectedFilters = v
    },
  },
  actions: {
    updateSelectedFilters({ commit, dispatch }, v) {
      commit('setSelectedFilters', v)

      dispatch('search/loadTasks', null, { root: true })
    },
  },
}
