import ls from 'local-storage'

import { authorize, fetchUser, logout } from '@/api/auth'

export default {
  namespaced: true,
  state: () => ({
    user: null,
  }),
  getters: {
    userName: (state) => state.user?.username,
    userId: (state) => state.user?.id,
    isApproved: (state) => state.user?.approved,
  },
  mutations: {
    setUser(state, user) {
      state.user = user
    },
  },
  actions: {
    async authorize({ commit }, form) {
      const fetchedUser = await authorize(form)
      const user = fetchedUser.data

      const { token } = user

      ls.set('token', token)

      commit('setUser', user)
    },
    async fetchUser({ commit }) {
      const token = ls.get('token')

      if (token) {
        const fetchedUser = await fetchUser()
        const user = fetchedUser.data

        commit('setUser', user)
      }
    },
    async logout({ commit }) {
      await logout()

      ls.remove('token')

      commit('setUser', null)
    },
  },
}
