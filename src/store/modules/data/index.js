export default {
  namespaced: true,
  state: () => ({
    faculties: null,
    specialties: null,
    subjects: null,
  }),
  mutations: {
    setFaculties(state, faculties) {
      state.faculties = faculties
    },
    setSpecialties(state, specialties) {
      state.specialties = specialties
    },
    setSubjects(state, subjects) {
      state.subjects = subjects
    },
  },
  actions: {
    initData({ commit }, { faculties, specialties, subjects }) {
      commit('setFaculties', faculties)
      commit('setSpecialties', specialties)
      commit('setSubjects', subjects)
    },
  },
}
