import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

import auth from '@/store/modules/auth'
import data from '@/store/modules/data'
import search from '@/store/modules/search'

export default new Vuex.Store({
  state: {},
  mutations: {},
  actions: {},
  modules: {
    auth,
    data,
    search,
  },
})
