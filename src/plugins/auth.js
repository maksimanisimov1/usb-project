import ls from 'local-storage'
import Vue from 'vue'

import { httpClient } from '@/api/httpClient'
import store from '@/store'

const authPlugin = {
  async install() {
    const token = ls.get('token')

    httpClient.interceptors.request.use(async (config) => {
      const token = ls.get('token')

      if (token) {
        return {
          ...config,
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      }

      return config
    })

    if (token) {
      await store.dispatch('auth/fetchUser')
    }
  },
}

Vue.use(authPlugin)

export default authPlugin
