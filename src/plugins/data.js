import Vue from 'vue'

import { fetchFaculties, fetchSpecialties, fetchSubjects } from '@/api/data'
import store from '@/store'

const dataPlugin = {
  async install() {
    const fetchedFaculties = await fetchFaculties()
    const fetchedSpecialties = await fetchSpecialties()
    const fetchedSubjects = await fetchSubjects()

    const faculties = fetchedFaculties.data
    const specialties = fetchedSpecialties.data
    const subjects = fetchedSubjects.data

    await store.dispatch('data/initData', { faculties, specialties, subjects })
  },
}

Vue.use(dataPlugin)

export default dataPlugin
