import Vue from 'vue'

import App from '@/App.vue'
import auth from '@/plugins/auth'
import data from '@/plugins/data'
import vuetify from '@/plugins/vuetify'
import router from '@/router'
import store from '@/store'

Vue.config.productionTip = false

new Vue({
  router,
  store,
  vuetify,
  auth,
  data,
  render: (h) => h(App),
}).$mount('#app')
