import { httpClient } from '@/api/httpClient'

const dataPath = '/data'

export const fetchFaculties = (payload) =>
  httpClient.get(`${dataPath}/faculty`, payload)

export const fetchSpecialties = (payload) =>
  httpClient.get(`${dataPath}/specialty`, payload)

export const fetchSubjects = (payload) =>
  httpClient.get(`${dataPath}/subject`, payload)

export const loadTask = (...payload) =>
  httpClient.post(`${dataPath}/save`, ...payload)

export const searchTasks = (payload) =>
  httpClient.get(`${dataPath}/search`, payload)

export const fetchTaskDownloadLink = (payload) =>
  httpClient.get(`${dataPath}/downloadLink`, payload)

export const likeTask = (payload) =>
  httpClient.post(`${dataPath}/like`, payload)

export const fetchTask = (payload) =>
  httpClient.get(`${dataPath}/fetchTask`, payload)
