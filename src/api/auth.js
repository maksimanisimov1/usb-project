import { httpClient } from '@/api/httpClient'

const authPath = '/auth'

export const register = (...payload) =>
  httpClient.post(`${authPath}/register`, ...payload)

export const authorize = (...payload) =>
  httpClient.post(`${authPath}/login`, ...payload)

export const logout = (...payload) =>
  httpClient.post(`${authPath}/logout`, ...payload)

export const fetchUser = (payload) =>
  httpClient.get(`${authPath}/fetchUser`, payload)
