import axios from 'axios'

const baseURL = `${process.env.VUE_APP_BASE_DOMAIN}/api`

const config = {
  baseURL,
}

export const httpClient = axios.create(config)
