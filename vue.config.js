const target = process.env.VUE_APP_PROXY_DOMAIN

module.exports = {
  lintOnSave: false,
  transpileDependencies: ['vuetify'],
  devServer: {
    proxy: {
      '/api': {
        target,
        changeOrigin: true,
      },
    },
  },
}
